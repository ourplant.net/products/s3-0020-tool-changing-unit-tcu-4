Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |  [de](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/01_operating_manual/S3-0020_A3_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/01_operating_manual/S3-0020_A1_Operating%20Manual.pdf)                     |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/02_assembly_drawing/s3-0020-B_tcu-4.PDF)                  |
| circuit diagram          |[de](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/03_circuit_diagram/S3-0020-EPLAN-B.pdf)                  |
| maintenance instructions | [de](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/04_maintenance_instructions/S3-0020_A_Wartungsanweisungen.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/04_maintenance_instructions/S3-0020_A_Maintenance_instructions.pdf)                  |
| spare parts              | [de](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/05_spare_parts/S3-0020_B_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0020-tool-changing-unit-tcu-4/-/raw/main/05_spare_parts/S3-0020_A1_EVL_engl.pdf)                  |

